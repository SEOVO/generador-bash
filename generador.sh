echo ' ' > temp.txt
echo '<?xml version="1.0" encoding="utf-8"?>' >> temp.txt
echo '<odoo>' >> temp.txt
echo '  <data noupdate="1">' >> temp.txt
while IFS= read -r line
 do
  IFS=','
  read -a strarr <<< "$line"
  #wget ${strarr[0]} -O ${strarr[1]//[[:space:]]}.webp
  echo '      <record id="apps_qr_'${strarr[1]//[[:space:]]}'" model="payment.appsqr">' >> temp.txt
  echo '        <field name="name">'${strarr[1]//[[:space:]]}'</field>' >> temp.txt
  echo '        <field name="icono" type="base64" file="odoo_pagos_qr/static/src/img/'${strarr[1]//[[:space:]]}'.webp" />' >> temp.txt
  echo '        <field name="cant_max">500</field>' >> temp.txt
  echo '        <field name="qrpagos" ref="odoo_pagos_qr.payment_acquirer_qrpagos" />' >> temp.txt
  echo '        <field name="paises" ' >> temp.txt
  echo "               eval='[(6, 0, [  " >> temp.txt
  if [ "${strarr[2]//[[:space:]]}" != "" ]; then
     echo '                      ref("'base.va'"),' >> temp.txt
  fi
  if [ "${strarr[3]//[[:space:]]}" != "" ]; then
     echo '                      ref("'base.mc'"),' >> temp.txt
  fi
  if [ "${strarr[4]//[[:space:]]}" != "" ]; then
     echo '                      ref("'base.nr'"),' >> temp.txt
  fi
  if [ "${strarr[6]//[[:space:]]}" != "" ]; then
     echo '                      ref("'base.tv'"),' >> temp.txt
  fi
  if [ "${strarr[7]//[[:space:]]}" != "" ]; then
     echo '                      ref("'base.mo'"),' >> temp.txt
  fi
  if [ "${strarr[8]//[[:space:]]}" != "" ]; then
     echo '                      ref("'base.bm'"),' >> temp.txt
  fi
  if [ "${strarr[9]}" != "" ]; then
     echo '                      ref("'base.sm'"),' >> temp.txt
  fi
  if [ "${strarr[10]}" != "" ]; then
     echo '                      ref("'base.ms'"),' >> temp.txt
  fi
  if [ "${strarr[11]}" != "" ]; then
     echo '                      ref("'base.vg'"),' >> temp.txt
  fi
  if [ "${strarr[12]}" != "" ]; then
     echo '                      ref("'base.li'"),' >> temp.txt
  fi
  if [ "${strarr[13]}" != "" ]; then
     echo '                      ref("'base.aw'"),' >> temp.txt
  fi
  if [ "${strarr[14]}" != "" ]; then
     echo '                      ref("'base.mh'"),' >> temp.txt
  fi
  if [ "${strarr[15]}" != "" ]; then
     echo '                      ref("'base.ck'"),' >> temp.txt
  fi
  if [ "${strarr[16]}" != "" ]; then
     echo '                      ref("'base.pm'"),' >> temp.txt
  fi
  if [ "${strarr[17]}" != "" ]; then
     echo '                      ref("'base.nu'"),' >> temp.txt
  fi
  if [ "${strarr[18]}" != "" ]; then
     echo '                      ref("'base.ky'"),' >> temp.txt
  fi
  if [ "${strarr[19]}" != "" ]; then
     echo '                      ref("'base.mv'"),' >> temp.txt
  fi
  if [ "${strarr[20]}" != "" ]; then
     echo '                      ref("'base.mt'"),' >> temp.txt
  fi
  if [ "${strarr[21]}" != "" ]; then
     echo '                      ref("'base.gd'"),' >> temp.txt
  fi
  if [ "${strarr[22]}" != "" ]; then
     echo '                      ref("'base.vc'"),' >> temp.txt
  fi
  if [ "${strarr[23]}" != "" ]; then
     echo '                      ref("'base.bb'"),' >> temp.txt
  fi
  if [ "${strarr[24]}" != "" ]; then
     echo '                      ref("'base.ag'"),' >> temp.txt
  fi
  if [ "${strarr[25]}" != "" ]; then
     echo '                      ref("'base.sc'"),' >> temp.txt
  fi
  if [ "${strarr[26]}" != "" ]; then
     echo '                      ref("'base.ad'"),' >> temp.txt
  fi
  if [ "${strarr[27]}" != "" ]; then
     echo '                      ref("'base.lc'"),' >> temp.txt
  fi
  if [ "${strarr[28]}" != "" ]; then
     echo '                      ref("'base.sg'"),' >> temp.txt
  fi
  if [ "${strarr[29]}" != "" ]; then
     echo '                      ref("'base.fm'"),' >> temp.txt
  fi
  if [ "${strarr[30]}" != "" ]; then
     echo '                      ref("'base.bh'"),' >> temp.txt
  fi
  if [ "${strarr[31]}" != "" ]; then
     echo '                      ref("'base.th'"),' >> temp.txt
  fi
  if [ "${strarr[32]}" != "" ]; then
     echo '                      ref("'base.dm'"),' >> temp.txt
  fi
  if [ "${strarr[33]}" != "" ]; then
     echo '                      ref("'base.ki'"),' >> temp.txt
  fi
  if [ "${strarr[34]}" != "" ]; then
     echo '                      ref("'base.hk'"),' >> temp.txt
  fi
  if [ "${strarr[35]}" != "" ]; then
     echo '                      ref("'base.mu'"),' >> temp.txt
  fi
  if [ "${strarr[36]}" != "" ]; then
     echo '                      ref("'base.lu'"),' >> temp.txt
  fi
  if [ "${strarr[37]}" != "" ]; then
     echo '                      ref("'base.tt'"),' >> temp.txt
  fi
  if [ "${strarr[38]}" != "" ]; then
     echo '                      ref("'base.pr'"),' >> temp.txt
  fi
  if [ "${strarr[39]}" != "" ]; then
     echo '                      ref("'base.jm'"),' >> temp.txt
  fi
  if [ "${strarr[40]}" != "" ]; then
     echo '                      ref("'base.qa'"),' >> temp.txt
  fi
  if [ "${strarr[41]}" != "" ]; then
     echo '                      ref("'base.me'"),' >> temp.txt
  fi
  if [ "${strarr[42]}" != "" ]; then
     echo '                      ref("'base.bs'"),' >> temp.txt
  fi
  if [ "${strarr[44]}" != "" ]; then
     echo '                      ref("'base.fj'"),' >> temp.txt
  fi
  if [ "${strarr[46]}" != "" ]; then
     echo '                      ref("'base.nc'"),' >> temp.txt
  fi
  if [ "${strarr[47]}" != "" ]; then
     echo '                      ref("'base.si'"),' >> temp.txt
  fi
  if [ "${strarr[48]}" != "" ]; then
     echo '                      ref("'base.sv'"),' >> temp.txt
  fi
  if [ "${strarr[49]}" != "" ]; then
     echo '                      ref("'base.il'"),' >> temp.txt
  fi
  if [ "${strarr[50]}" != "" ]; then
     echo '                      ref("'base.bz'"),' >> temp.txt
  fi
  if [ "${strarr[51]}" != "" ]; then
     echo '                      ref("'base.mk'"),' >> temp.txt
  fi
  if [ "${strarr[52]}" != "" ]; then
     echo '                      ref("'base.rw'"),' >> temp.txt
  fi
  if [ "${strarr[53]}" != "" ]; then
     echo '                      ref("'base.ht'"),' >> temp.txt
  fi
  if [ "${strarr[54]}" != "" ]; then
     echo '                      ref("'base.al'"),' >> temp.txt
  fi
  if [ "${strarr[55]}" != "" ]; then
     echo '                      ref("'base.sb'"),' >> temp.txt
  fi
  if [ "${strarr[56]}" != "" ]; then
     echo '                      ref("'base.am'"),' >> temp.txt
  fi
  if [ "${strarr[57]}" != "" ]; then
     echo '                      ref("'base.ls'"),' >> temp.txt
  fi
  if [ "${strarr[58]}" != "" ]; then
     echo '                      ref("'base.be'"),' >> temp.txt
  fi
  if [ "${strarr[59]}" != "" ]; then
     echo '                      ref("'base.md'"),' >> temp.txt
  fi
  if [ "${strarr[60]}" != "" ]; then
     echo '                      ref("'base.gw'"),' >> temp.txt
  fi
  if [ "${strarr[61]}" != "" ]; then
     echo '                      ref("'base.bt'"),' >> temp.txt
  fi
  if [ "${strarr[62]}" != "" ]; then
     echo '                      ref("'base.ch'"),' >> temp.txt
  fi
  if [ "${strarr[63]}" != "" ]; then
     echo '                      ref("'base.nl'"),' >> temp.txt
  fi
  if [ "${strarr[64]}" != "" ]; then
     echo '                      ref("'base.dk'"),' >> temp.txt
  fi
  if [ "${strarr[65]}" != "" ]; then
     echo '                      ref("'base.ee'"),' >> temp.txt
  fi
  if [ "${strarr[66]}" != "" ]; then
     echo '                      ref("'base.do'"),' >> temp.txt
  fi
   if [ "${strarr[67]}" != "" ]; then
     echo '                      ref("'base.sk'"),' >> temp.txt
  fi
  if [ "${strarr[68]}" != "" ]; then
     echo '                      ref("'base.cr'"),' >> temp.txt
  fi
  if [ "${strarr[69]}" != "" ]; then
     echo '                      ref("'base.ba'"),' >> temp.txt
  fi
  if [ "${strarr[70]}" != "" ]; then
     echo '                      ref("'base.hr'"),' >> temp.txt
  fi
  if [ "${strarr[71]}" != "" ]; then
     echo '                      ref("'base.tg'"),' >> temp.txt
  fi
  if [ "${strarr[72]}" != "" ]; then
     echo '                      ref("'base.lv'"),' >> temp.txt
  fi
  if [ "${strarr[73]}" != "" ]; then
     echo '                      ref("'base.lt'"),' >> temp.txt
  fi
  if [ "${strarr[74]}" != "" ]; then
     echo '                      ref("'base.lk'"),' >> temp.txt
  fi
   if [ "${strarr[75]}" != "" ]; then
     echo '                      ref("'base.ge'"),' >> temp.txt
  fi
  if [ "${strarr[76]}" != "" ]; then
     echo '                      ref("'base.ie'"),' >> temp.txt
  fi
  if [ "${strarr[77]}" != "" ]; then
     echo '                      ref("'base.sl'"),' >> temp.txt
  fi
  if [ "${strarr[78]}" != "" ]; then
     echo '                      ref("'base.pa'"),' >> temp.txt
  fi
  if [ "${strarr[79]}" != "" ]; then
     echo '                      ref("'base.rs'"),' >> temp.txt
  fi
  if [ "${strarr[80]}" != "" ]; then
     echo '                      ref("'base.cz'"),' >> temp.txt
  fi
  if [ "${strarr[81]}" != "" ]; then
     echo '                      ref("'base.ae'"),' >> temp.txt
  fi
  if [ "${strarr[82]}" != "" ]; then
     echo '                      ref("'base.at'"),' >> temp.txt
  fi
  if [ "${strarr[83]}" != "" ]; then
     echo '                      ref("'base.az'"),' >> temp.txt
  fi
  if [ "${strarr[84]}" != "" ]; then
     echo '                      ref("'base.jo'"),' >> temp.txt
  fi
  if [ "${strarr[85]}" != "" ]; then
     echo '                      ref("'base.pt'"),' >> temp.txt
  fi
  if [ "${strarr[86]}" != "" ]; then
     echo '                      ref("'base.hu'"),' >> temp.txt
  fi
  if [ "${strarr[87]}" != "" ]; then
     echo '                      ref("'base.kr'"),' >> temp.txt
  fi
  if [ "${strarr[88]}" != "" ]; then
     echo '                      ref("'base.is'"),' >> temp.txt
  fi
  if [ "${strarr[89]}" != "" ]; then
     echo '                      ref("'base.gt'"),' >> temp.txt
  fi
  if [ "${strarr[90]}" != "" ]; then
     echo '                      ref("'base.bg'"),' >> temp.txt
  fi
  if [ "${strarr[91]}" != "" ]; then
     echo '                      ref("'base.hn'"),' >> temp.txt
  fi
  if [ "${strarr[92]}" != "" ]; then
     echo '                      ref("'base.bj'"),' >> temp.txt
  fi
  if [ "${strarr[93]}" != "" ]; then
     echo '                      ref("'base.er'"),' >> temp.txt
  fi
  if [ "${strarr[94]}" != "" ]; then
     echo '                      ref("'base.mw'"),' >> temp.txt
  fi
  if [ "${strarr[95]}" != "" ]; then
     echo '                      ref("'base.ni'"),' >> temp.txt
  fi
  if [ "${strarr[96]}" != "" ]; then
     echo '                      ref("'base.gr'"),' >> temp.txt
  fi
  if [ "${strarr[97]}" != "" ]; then
     echo '                      ref("'base.tj'"),' >> temp.txt
  fi
  if [ "${strarr[98]}" != "" ]; then
     echo '                      ref("'base.bd'"),' >> temp.txt
  fi
  if [ "${strarr[99]}" != "" ]; then
     echo '                      ref("'base.np'"),' >> temp.txt
  fi
  if [ "${strarr[100]}" != "" ]; then
     echo '                      ref("'base.tn'"),' >> temp.txt
  fi
  if [ "${strarr[101]}" != "" ]; then
     echo '                      ref("'base.sr'"),' >> temp.txt
  fi
  if [ "${strarr[102]}" != "" ]; then
     echo '                      ref("'base.uy'"),' >> temp.txt
  fi
  if [ "${strarr[103]}" != "" ]; then
     echo '                      ref("'base.kh'"),' >> temp.txt
  fi
  if [ "${strarr[104]}" != "" ]; then
     echo '                      ref("'base.sn'"),' >> temp.txt
  fi
  if [ "${strarr[105]}" != "" ]; then
     echo '                      ref("'base.kg'"),' >> temp.txt
  fi
  if [ "${strarr[106]}" != "" ]; then
     echo '                      ref("'base.by'"),' >> temp.txt
  fi
  if [ "${strarr[107]}" != "" ]; then
     echo '                      ref("'base.gy'"),' >> temp.txt
  fi
  if [ "${strarr[108]}" != "" ]; then
     echo '                      ref("'base.la'"),' >> temp.txt
  fi
  if [ "${strarr[109]}" != "" ]; then
     echo '                      ref("'base.ro'"),' >> temp.txt
  fi
  if [ "${strarr[110]}" != "" ]; then
     echo '                      ref("'base.ug'"),' >> temp.txt
  fi
  if [ "${strarr[111]}" != "" ]; then
     echo '                      ref("'base.uk'"),' >> temp.txt
  fi
  if [ "${strarr[112]}" != "" ]; then
     echo '                      ref("'base.gn'"),' >> temp.txt
  fi
  if [ "${strarr[113]}" != "" ]; then
     echo '                      ref("'base.ga'"),' >> temp.txt
  fi
  if [ "${strarr[114]}" != "" ]; then
     echo '                      ref("'base.nz'"),' >> temp.txt
  fi
  if [ "${strarr[115]}" != "" ]; then
     echo '                      ref("'base.bf'"),' >> temp.txt
  fi
  if [ "${strarr[116]}" != "" ]; then
     echo '                      ref("'base.ec'"),' >> temp.txt
  fi
  if [ "${strarr[117]}" != "" ]; then
     echo '                      ref("'base.ph'"),' >> temp.txt
  fi
  if [ "${strarr[118]}" != "" ]; then
     echo '                      ref("'base.it'"),' >> temp.txt
  fi
  if [ "${strarr[119]}" != "" ]; then
     echo '                      ref("'base.om'"),' >> temp.txt
  fi
  if [ "${strarr[120]}" != "" ]; then
     echo '                      ref("'base.pl'"),' >> temp.txt
  fi
  if [ "${strarr[121]}" != "" ]; then
     echo '                      ref("'base.ci'"),' >> temp.txt
  fi
  if [ "${strarr[122]}" != "" ]; then
     echo '                      ref("'base.no'"),' >> temp.txt
  fi
  if [ "${strarr[123]}" != "" ]; then
     echo '                      ref("'base.my'"),' >> temp.txt
  fi
  if [ "${strarr[124]}" != "" ]; then
     echo '                      ref("'base.vn'"),' >> temp.txt
  fi
  if [ "${strarr[125]}" != "" ]; then
     echo '                      ref("'base.fi'"),' >> temp.txt
  fi
  if [ "${strarr[126]}" != "" ]; then
     echo '                      ref("'base.de'"),' >> temp.txt
  fi
  if [ "${strarr[127]//[[:space:]]}" != "" ]; then
     echo '                      ref("'base.ru'"),' >> temp.txt
  fi
  if [ "${strarr[128]}" != "" ]; then
     echo '                      ref("'base.ca'"),' >> temp.txt
  fi
  if [ "${strarr[129]}" != "" ]; then
     echo '                      ref("'base.cn'"),' >> temp.txt
  fi
  if [ "${strarr[130]}" != "" ]; then
     echo '                      ref("'base.us'"),' >> temp.txt
  fi
  if [ "${strarr[131]}" != "" ]; then
     echo '                      ref("'base.br'"),' >> temp.txt
  fi
  if [ "${strarr[132]}" != "" ]; then
     echo '                      ref("'base.au'"),' >> temp.txt
  fi
  if [ "${strarr[133]}" != "" ]; then
     echo '                      ref("'base.in'"),' >> temp.txt
  fi
  if [ "${strarr[134]}" != "" ]; then
     echo '                      ref("'base.ar'"),' >> temp.txt
  fi
  if [ "${strarr[135]}" != "" ]; then
     echo '                      ref("'base.kz'"),' >> temp.txt
  fi
  if [ "${strarr[136]}" != "" ]; then
     echo '                      ref("'base.cg'"),' >> temp.txt
  fi
  if [ "${strarr[137]}" != "" ]; then
     echo '                      ref("'base.sa'"),' >> temp.txt
  fi
  if [ "${strarr[138]}" != "" ]; then
     echo '                      ref("'base.mx'"),' >> temp.txt
  fi
  if [ "${strarr[139]}" != "" ]; then
     echo '                      ref("'base.id'"),' >> temp.txt
  fi
  if [ "${strarr[140]}" != "" ]; then
     echo '                      ref("'base.mn'"),' >> temp.txt
  fi
  if [ "${strarr[141]}" != "" ]; then
     echo '                      ref("'base.pe'"),' >> temp.txt
  fi
  if [ "${strarr[142]}" != "" ]; then
     echo '                      ref("'base.td'"),' >> temp.txt
  fi
  if [ "${strarr[143]}" != "" ]; then
     echo '                      ref("'base.ne'"),' >> temp.txt
  fi
  if [ "${strarr[144]}" != "" ]; then
     echo '                      ref("'base.ao'"),' >> temp.txt
  fi
  if [ "${strarr[145]}" != "" ]; then
     echo '                      ref("'base.ml'"),' >> temp.txt
  fi
  if [ "${strarr[146]}" != "" ]; then
     echo '                      ref("'base.ss'"),' >> temp.txt
  fi
  if [ "${strarr[147]}" != "" ]; then
     echo '                      ref("'base.co'"),' >> temp.txt
  fi
  if [ "${strarr[148]}" != "" ]; then
     echo '                      ref("'base.et'"),' >> temp.txt
  fi
  if [ "${strarr[149]}" != "" ]; then
     echo '                      ref("'base.bo'"),' >> temp.txt
  fi
  if [ "${strarr[150]}" != "" ]; then
     echo '                      ref("'base.mr'"),' >> temp.txt
  fi
  if [ "${strarr[151]}" != "" ]; then
     echo '                      ref("'base.eg'"),' >> temp.txt
  fi
  if [ "${strarr[152]}" != "" ]; then
     echo '                      ref("'base.tz'"),' >> temp.txt
  fi
  if [ "${strarr[153]}" != "" ]; then
     echo '                      ref("'base.ne'"),' >> temp.txt
  fi
  if [ "${strarr[154]}" != "" ]; then
     echo '                      ref("'base.ve'"),' >> temp.txt
  fi
  if [ "${strarr[155]}" != "" ]; then
     echo '                      ref("'base.na'"),' >> temp.txt
  fi
  if [ "${strarr[156]}" != "" ]; then
     echo '                      ref("'base.mz'"),' >> temp.txt
  fi
  if [ "${strarr[157]}" != "" ]; then
     echo '                      ref("'base.tr'"),' >> temp.txt
  fi
  if [ "${strarr[158]}" != "" ]; then
     echo '                      ref("'base.cl'"),' >> temp.txt
  fi
  if [ "${strarr[159]}" != "" ]; then
     echo '                      ref("'base.zm'"),' >> temp.txt
  fi
  if [ "${strarr[160]}" != "" ]; then
     echo '                      ref("'base.mm'"),' >> temp.txt
  fi
  if [ "${strarr[161]}" != "" ]; then
     echo '                      ref("'base.fr'"),' >> temp.txt
  fi
  if [ "${strarr[162]}" != "" ]; then
     echo '                      ref("'base.so'"),' >> temp.txt
  fi
  if [ "${strarr[163]}" != "" ]; then
     echo '                      ref("'base.ua'"),' >> temp.txt
  fi
  if [ "${strarr[164]}" != "" ]; then
     echo '                      ref("'base.mg'"),' >> temp.txt
  fi
  if [ "${strarr[165]}" != "" ]; then
     echo '                      ref("'base.bw'"),' >> temp.txt
  fi
  if [ "${strarr[166]}" != "" ]; then
     echo '                      ref("'base.ke'"),' >> temp.txt
  fi
  if [ "${strarr[167]}" != "" ]; then
     echo '                      ref("'base.ye'"),' >> temp.txt
  fi
  if [ "${strarr[168]}" != "" ]; then
     echo '                      ref("'base.th'"),' >> temp.txt
  fi
  if [ "${strarr[169]}" != "" ]; then
     echo '                      ref("'base.es'"),' >> temp.txt
  fi
  if [ "${strarr[170]}" != "" ]; then
     echo '                      ref("'base.tm'"),' >> temp.txt
  fi
  if [ "${strarr[171]}" != "" ]; then
     echo '                      ref("'base.cm'"),' >> temp.txt
  fi
  if [ "${strarr[172]}" != "" ]; then
     echo '                      ref("'base.gn'"),' >> temp.txt
  fi
  if [ "${strarr[173]}" != "" ]; then
     echo '                      ref("'base.se'"),' >> temp.txt
  fi
  if [ "${strarr[174]}" != "" ]; then
     echo '                      ref("'base.ma'"),' >> temp.txt
  fi
  if [ "${strarr[175]}" != "" ]; then
     echo '                      ref("'base.py'"),' >> temp.txt
  fi
  if [ "${strarr[176]}" != "" ]; then
     echo '                      ref("'base.zw'"),' >> temp.txt
  fi
  if [ "${strarr[177]}" != "" ]; then
     echo '                      ref("'base.jp'"),' >> temp.txt
  fi
  echo "                      ])]' />" >> temp.txt
  echo '      </record>' >> temp.txt
 done < data.csv
echo '  </data>' >> temp.txt
echo '</odoo>' >> temp.txt
